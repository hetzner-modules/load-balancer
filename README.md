<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.2.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.0 |
| <a name="requirement_hcloud"></a> [hcloud](#requirement\_hcloud) | >= 1.33.2 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 4.0 |
| <a name="provider_hcloud"></a> [hcloud](#provider\_hcloud) | >= 1.33.2 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_route53_record.rancher_record](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [hcloud_firewall.server](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/firewall) | resource |
| [hcloud_firewall_attachment.kubectl](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/firewall_attachment) | resource |
| [hcloud_load_balancer.load_balancer](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/load_balancer) | resource |
| [hcloud_load_balancer_network.network](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/load_balancer_network) | resource |
| [hcloud_load_balancer_service.service](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/load_balancer_service) | resource |
| [hcloud_load_balancer_target.labels](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/load_balancer_target) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_algorithm_type"></a> [algorithm\_type](#input\_algorithm\_type) | (string) Type of the Load Balancer Algorithm. round\_robin or least\_connections | `string` | `"round_robin"` | no |
| <a name="input_firewall_ports"></a> [firewall\_ports](#input\_firewall\_ports) | firewall ports to open for servers attached to load balancer | `list(string)` | `[]` | no |
| <a name="input_hcloud_location"></a> [hcloud\_location](#input\_hcloud\_location) | Hetzner location where load balancer should get placed | `string` | `"nbg1"` | no |
| <a name="input_label_selector"></a> [label\_selector](#input\_label\_selector) | The label selector being used to balance the load to | `list(string)` | `[]` | no |
| <a name="input_lb_name"></a> [lb\_name](#input\_lb\_name) | Name of the load balancer | `string` | n/a | yes |
| <a name="input_load_balancer_target_type"></a> [load\_balancer\_target\_type](#input\_load\_balancer\_target\_type) | (Required, string) Type of the target. Possible values server, label\_selector, ip. | `string` | `"server"` | no |
| <a name="input_load_balancer_type"></a> [load\_balancer\_type](#input\_load\_balancer\_type) | (Required, string) Type of the Load Balancer. | `string` | `"lb11"` | no |
| <a name="input_route53_records"></a> [route53\_records](#input\_route53\_records) | The route53 records to create for load balancer | `list(string)` | `[]` | no |
| <a name="input_route53_zone_id"></a> [route53\_zone\_id](#input\_route53\_zone\_id) | Zone Id of route53 zone that creates A record for load balancer | `string` | n/a | yes |
| <a name="input_server_ids"></a> [server\_ids](#input\_server\_ids) | IDs of servers to attach to the load balancer | `set(string)` | `[]` | no |
| <a name="input_service_ports"></a> [service\_ports](#input\_service\_ports) | server ports that the load balancer forwards traffic to | `list(string)` | `[]` | no |
| <a name="input_subnet_id"></a> [subnet\_id](#input\_subnet\_id) | Id of the network to place load balancer in | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_ipv4"></a> [ipv4](#output\_ipv4) | n/a |
| <a name="output_lb_location"></a> [lb\_location](#output\_lb\_location) | n/a |
| <a name="output_rancher_record"></a> [rancher\_record](#output\_rancher\_record) | n/a |
<!-- END_TF_DOCS -->
