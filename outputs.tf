output "ipv4" {
  value = hcloud_load_balancer.load_balancer.ipv4
}

output "rancher_record" {
  value = aws_route53_record.rancher_record[*].name
}

output "lb_location" {
  value = hcloud_load_balancer.load_balancer.location
}