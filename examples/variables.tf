variable "server_name" {
  type        = string
  description = "(Required, string) Name of the server to create (must be unique per project and a valid hostname as per RFC 1123)."
}

variable "server_type" {
  type        = string
  description = "(Required, string) Name of the server type this server should be created with."
}

variable "image_name" {
  type        = string
  description = "(Required, string) Name or ID of the image the server is created from. Note the image property is only required when using the resource to create servers. As the Hetzner Cloud API may return servers without an image ID set it is not marked as required in the Terraform Provider itself. Thus, users will get an error from the underlying client library if they forget to set the property and try to create a server."
}

variable "network_ip_range" {
  type        = string
  description = "(Required, string) IP Range of the whole Network which must span all included subnets and route destinations. Must be one of the private ipv4 ranges of RFC1918."
}

variable "network_name" {
  type        = string
  description = "(Required, string) Name of the Network to create (must be unique per project)."
}

variable "network_cidr" {
  type        = string
  description = "Network to create for private communication"
}

variable "hcloud_token" {
  sensitive   = true
  type        = string
  description = "Hetzner Cloud API token used to create infrastructure"
}

variable "hcloud_location" {
  type        = string
  description = "Hetzner location where load balancer should get placed"
  default     = "nbg1"
}