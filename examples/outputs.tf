output "ipv4" {
  value = module.load_balancer.ipv4
}

output "location" {
  value = module.load_balancer.lb_location
}