terraform {
  required_version = ">= 1.2.0"
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/52559871/terraform/state/load-balancer"
    lock_address   = "https://gitlab.com/api/v4/projects/52559871/terraform/state/load-balancer/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/52559871/terraform/state/load-balancer/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.38.0"
    }
  }
}

provider "hcloud" {
  token = var.hcloud_token
}

provider "aws" {
  region = "eu-central-1"
}

module "network" {
  source           = "git::https://gitlab.com/hetzner-modules/network.git"
  network_ip_range = var.network_cidr
  network_name     = var.network_name
  subnet_ip_ranges = [var.network_ip_range]
}

module "server" {
  source      = "git::https://gitlab.com/hetzner-modules/server.git"
  image_name  = var.image_name
  network_id  = module.network.network_id
  server_name = var.server_name
  server_type = var.server_type
  user_data   = templatefile("${path.root}/../scripts/userdata.template", {})
}

module "load_balancer" {
  source          = "../"
  lb_name         = "test-load-balancer"
  hcloud_location = var.hcloud_location
  subnet_id       = module.network.network_subnet_id[0]
  service_ports   = ["80"]
  route53_zone_id = ""
  label_selector  = ["hcloud/node-group=${var.server_name}", "hcloud/node-group=${var.server_name}"]
}