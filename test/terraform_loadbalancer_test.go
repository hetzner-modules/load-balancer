package test

import (
	"fmt"
	"os"
	"testing"
	"time"

	http_helper "github.com/gruntwork-io/terratest/modules/http-helper"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

func TestLoadBalancer(t *testing.T) {
	t.Parallel()

	terraformOptions := &terraform.Options{
		TerraformDir: "../examples",
		VarFiles:     []string{"terraform.tfvars"},
		BackendConfig: map[string]interface{}{
			"username": os.Getenv("TF_USERNAME"),
			"password": os.Getenv("ACCESS_TOKEN"),
		},
	}

	defer terraform.Destroy(t, terraformOptions)
	terraform.InitAndApply(t, terraformOptions)

	lb_ipv4 := terraform.Output(t, terraformOptions, "ipv4")
	lb_location := terraform.Output(t, terraformOptions, "location")
	url := fmt.Sprintf("http://%s", lb_ipv4)

	time.Sleep(60 * time.Second)

	assert.Equal(t, "nbg1", lb_location)
	assert.NotEmpty(t, lb_ipv4, "Load balancer does not have an IP address")
	http_helper.HttpGet(t, url, nil)
}
