variable "lb_name" {
  type        = string
  description = "Name of the load balancer"
}

variable "subnet_id" {
  type        = string
  description = "Id of the network to place load balancer in"
}

variable "hcloud_location" {
  type        = string
  description = "Hetzner location where load balancer should get placed"
  default     = "nbg1"
}

variable "server_ids" {
  type        = set(string)
  description = "IDs of servers to attach to the load balancer"
  default     = []
}

variable "route53_zone_id" {
  type        = string
  description = "Zone Id of route53 zone that creates A record for load balancer"
}

variable "route53_records" {
  type        = list(string)
  description = "The route53 records to create for load balancer"
  default     = []
}

variable "label_selector" {
  type        = list(string)
  description = "The label selector being used to balance the load to"
  default     = []
}

variable "algorithm_type" {
  type        = string
  description = "(string) Type of the Load Balancer Algorithm. round_robin or least_connections"
  default     = "round_robin"
}

variable "load_balancer_type" {
  type        = string
  description = "(Required, string) Type of the Load Balancer."
  default     = "lb11"
}

variable "load_balancer_target_type" {
  type        = string
  description = "(Required, string) Type of the target. Possible values server, label_selector, ip."
  default     = "server"
}

variable "firewall_ports" {
  type        = list(string)
  description = "firewall ports to open for servers attached to load balancer"
  default     = []
}

variable "service_ports" {
  type        = list(string)
  description = "server ports that the load balancer forwards traffic to"
  default     = []
}