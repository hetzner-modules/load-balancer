resource "hcloud_load_balancer" "load_balancer" {
  load_balancer_type = var.load_balancer_type
  name               = var.lb_name
  location           = var.hcloud_location
  algorithm {
    type = var.algorithm_type
  }
  dynamic "target" {
    for_each = var.server_ids
    content {
      type           = var.load_balancer_target_type
      server_id      = target.value
      use_private_ip = false
    }
  }
}

resource "hcloud_load_balancer_target" "labels" {
  count            = length(var.label_selector) > 0 ? length(var.label_selector) : 0
  load_balancer_id = hcloud_load_balancer.load_balancer.id
  type             = "label_selector"
  label_selector   = var.label_selector[count.index]
}

resource "hcloud_load_balancer_service" "service" {
  for_each         = toset(concat(["443"], var.service_ports))
  load_balancer_id = hcloud_load_balancer.load_balancer.id
  protocol         = "tcp"
  listen_port      = each.value
  destination_port = each.value

  health_check {
    interval = "10"
    port     = each.value
    protocol = "tcp"
    timeout  = "10"
  }
}

resource "hcloud_load_balancer_network" "network" {
  load_balancer_id        = hcloud_load_balancer.load_balancer.id
  subnet_id               = var.subnet_id
  enable_public_interface = true
}

resource "aws_route53_record" "rancher_record" {
  count   = length(var.route53_records)
  zone_id = var.route53_zone_id
  name    = var.route53_records[count.index]
  type    = "A"
  ttl     = 300
  records = [hcloud_load_balancer.load_balancer.ipv4]
}

resource "hcloud_firewall" "server" {
  count = length(var.firewall_ports) > 0 ? 1 : 0
  name  = "server-firewall"
  dynamic "rule" {
    for_each = var.firewall_ports
    content {
      direction  = "in"
      protocol   = "tcp"
      port       = rule.value
      source_ips = ["${hcloud_load_balancer.load_balancer.ipv4}/32"]
    }
  }
}

resource "hcloud_firewall_attachment" "kubectl" {
  count       = length(var.firewall_ports) > 0 ? 1 : 0
  firewall_id = hcloud_firewall.server[0].id
  server_ids  = var.server_ids
}
