# [1.2.0](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-load-balancer/compare/1.1.9...1.2.0) (2023-04-19)


### Bug Fixes

* added hcloud location option ([8c97075](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-load-balancer/commit/8c9707528be57a6104d5861dbeed3dfbc5561199))
* updated hcloud location ([a2cd511](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-load-balancer/commit/a2cd511dc01613eb024b85875077960755135870))


### Features

* added testing and location to module ([0360260](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-load-balancer/commit/0360260ccc7961c795f319f6c0222117a7a704b0))
* added testing and location to module ([84b5008](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-load-balancer/commit/84b500862fa3de099a1a04f6e795b8cc51dbbe1e))
